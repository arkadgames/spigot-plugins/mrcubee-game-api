package fr.mrcubee.plugin.spigot.utils.tasks.listener;

/**
 * 
 * @author MrCubee
 *
 */
public interface TaskUpdate {
	
	public boolean update();

}
