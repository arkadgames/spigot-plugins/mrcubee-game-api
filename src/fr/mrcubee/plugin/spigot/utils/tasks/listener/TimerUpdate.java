package fr.mrcubee.plugin.spigot.utils.tasks.listener;

/**
 * 
 * @author MrCubee
 *
 */
public interface TimerUpdate {
	
	public boolean update(int current, int min, int max);

}
