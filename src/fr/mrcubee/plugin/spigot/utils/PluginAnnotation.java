package fr.mrcubee.plugin.spigot.utils;

import org.bukkit.plugin.Plugin;

import fr.mrcubee.plugin.spigot.utils.annotation.command.CommandAnnotationLoader;
import fr.mrcubee.plugin.spigot.utils.annotation.config.ConfigAnnotationLoader;
import fr.mrcubee.plugin.spigot.utils.annotation.manager.PluginAnnotationsLoader;

/**
 * 
 * @author MrCubee
 *
 */
public class PluginAnnotation {
	
	private static final ConfigAnnotationLoader CONFIG_LOADER = new ConfigAnnotationLoader();
	private static final CommandAnnotationLoader COMMAND_LOADER = new CommandAnnotationLoader();
	private static final PluginAnnotationsLoader[] INTERFACES_LOADER = {CONFIG_LOADER, COMMAND_LOADER};
	
	public static void loadAnnotations(Plugin plugin, Object... objects) {
		for (PluginAnnotationsLoader interfaceLoader : INTERFACES_LOADER)
			interfaceLoader.loadClass(plugin, objects);
	}
}
