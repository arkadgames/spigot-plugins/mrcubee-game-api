package fr.mrcubee.plugin.spigot.utils.annotation.command;

/**
 * 
 * @author MrCubee
 *
 */
public @interface CommandTabComplete {
	String command();
}
