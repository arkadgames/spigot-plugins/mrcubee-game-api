package fr.mrcubee.plugin.spigot.utils.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Command {
	String command();
	String usage();
	boolean color() default false;
	char colorChar() default '&';
	int minArguments() default 0;
	int maxArguments() default -1;
}
