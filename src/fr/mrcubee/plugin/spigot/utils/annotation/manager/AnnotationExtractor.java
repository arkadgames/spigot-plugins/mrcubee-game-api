package fr.mrcubee.plugin.spigot.utils.annotation.manager;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author MrCubee
 *
 */
public class AnnotationExtractor {
	
	public static <T extends Annotation> Map<Field, T> getDeclaredFields(Class<T> annotationClass, Object object) {
		Class<?> objectClass;
		HashMap<Field, T> result;
		T annotation;
		if (annotationClass == null || object == null)
			return null;
		objectClass = object.getClass();
		result = new HashMap<Field, T>();
		for (Field field :  objectClass.getDeclaredFields()) {
			annotation = field.getAnnotation(annotationClass);
			if (annotation != null)
				result.put(field, annotation);
		}
		return result;
	}
	
	public static <T extends Annotation> Map<Method, T> getDeclaredMethods(Class<T> annotationClass, Object object) {
		Class<?> objectClass;
		HashMap<Method, T> result;
		T annotation;
		if (annotationClass == null || object == null)
			return null;
		objectClass = object.getClass();
		result = new HashMap<Method, T>();
		for (Method method :  objectClass.getDeclaredMethods()) {
			annotation = method.getAnnotation(annotationClass);
			if (annotation != null)
				result.put(method, annotation);
		}
		return result;
	}

}
