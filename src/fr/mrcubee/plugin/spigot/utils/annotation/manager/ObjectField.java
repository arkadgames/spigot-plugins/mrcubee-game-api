package fr.mrcubee.plugin.spigot.utils.annotation.manager;

import java.lang.reflect.Field;

/**
 * 
 * @author MrCubee
 *
 */
public class ObjectField {
	private Object object;
	private Field field;
	
	public ObjectField(Object object, Field field) {
		this.object = object;
		this.field = field;
	}
	
	public Object getObject() {
		return object;
	}
	
	public Field getField() {
		return field;
	}
}
