package fr.mrcubee.plugin.spigot.utils.annotation.manager;

import org.bukkit.plugin.Plugin;

/**
 * 
 * @author MrCubee
 *
 */
public interface PluginAnnotationsLoader {
	
	public void loadClass(Plugin plugin, Object... objects);

}
