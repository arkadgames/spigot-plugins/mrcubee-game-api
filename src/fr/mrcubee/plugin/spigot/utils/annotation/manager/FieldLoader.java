package fr.mrcubee.plugin.spigot.utils.annotation.manager;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * 
 * @author MrCubee
 *
 * @param <T>
 */
public interface FieldLoader<T extends Annotation> {
	
	public void load(Field field, T annotation, Object object);

}
