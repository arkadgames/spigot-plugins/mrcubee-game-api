package fr.mrcubee.plugin.spigot.utils;

import org.bukkit.Bukkit;

/**
 * 
 * @author MrCubee
 *
 */
public class ServerVersion {

	public static String getPackageVersion() {
		return Bukkit.getServer().getClass().getPackage().getName()
				.substring(Bukkit.getServer().getClass().getPackage().getName().lastIndexOf(".") + 1);
	}

}
